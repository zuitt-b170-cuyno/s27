const http = require('http')
const port = 4000

const server = http.createServer((req, res) => {
    // HTTP method of the incoming request can be accessed via req.method property
    // GET method  retrieves/reads information
    if (req.url === "/items" && req.method === "GET") {
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Data retrieved from database")
    }
    else if (req.url === "/items" && req.method === "POST") {
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Data to be sent to the database")
    }
    else if (req.url === "/items" && req.method === "PUT") {
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Data has been updated")
    }
    else if (req.url === "/items" && req.method === "DELETE") {
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.end("Data has been deleted")
    }
})

server.listen(port)
console.log(`Server is running successfully at port:${port}`)