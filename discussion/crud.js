const http = require('http')
const port = 4000
const database = [
    {
        "name": "Brandon",
        "email": "brandon@mail.com"
    },
    {
        "name": "Jobert",
        "email": "jobert@mail.com"
    }
]

const server = http.createServer((req, res) => {
    // Route for returning all items upon recieving the GET request
    // Server to client - JSON.stringify(...)
    // Client to server - JSON.parse(...)
    // The data that will be recieved by the users/client from the server will be in the form of stringified JSON
    if (req.url === "/users" && req.method === "GET") {
        res.writeHead(200, {"Content-Type": "_application/json"})
        res.write(JSON.stringify(database))
        res.end()
    }
    if (req.url === "/users" && req.method === "POST") {
        let requestBody = ""
        /* 
            Data stream - sequence/flow of data
            Data step - data is recieved from the client and is processed in the stream called data
                where the code/statement will be triggered.
            End step - only runs after the request has been 
        */
        req.on("data", data => {
            requestBody += data
        })

        req.on("end", () => {
            requestBody = JSON.parse(requestBody)
            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }

            database.push(newUser)
            console.log(database)
            
            res.writeHead(200, {"Content-Type": "_application/json"})
            res.write(JSON.stringify(database))
            res.end()
        })
    }
})

server.listen(port)
console.log(`Server is running successfully at port:${port}`)