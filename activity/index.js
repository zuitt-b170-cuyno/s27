const http = require('http')
const port = 4000

const server = http.createServer((req, res) => {
    res.writeHead(200, {"Content-Type": "text/plain"})
    if (req.url === "/" && req.method === "GET")
        res.end("Welcome to Booking System")
    else if (req.url === "/profile" && req.method === "GET") 
        res.end("Welcome to your profile!")
    else if (req.url === "/courses" && req.method === "GET") 
        res.end("Here’s our courses available")
    else if (req.url === "/addCourse" && req.method === "POST") 
        res.end("Add a course to our resources")
    else if (req.url === "/updateCourse" && req.method === "PUT") 
        res.end("Update a course to our resources")
    else if (req.url === "/archieveCourse" && req.method === "DELETE") 
        res.end("Archive courses to our resources")
})

server.listen(port)
console.log(`Server is running successfully at port:${port}`)